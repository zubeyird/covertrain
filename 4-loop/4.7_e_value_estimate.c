
/**
 * Author: Zubeyir
 * Date:2023/1/2         
 * Source: Teacher Chris Bourke's book
 * Chapter 4: Loops
 * Example:Excercise 4.7
 * Aim: 
 * estimate the value of e=2.718281...
 */


#include<stdio.h>
#include<stdlib.h>


int main(int argc, char ** argv){
    int a=0;

    printf("Enter a number:");
    scanf("%d",&a);
    printf("\nEntered number:%d\n",a);
    
        int x=a;
   double sum=0; 
    for(int i=a;i>0;i--){

        double su=1;
        while(x>1){su*=x; 
            x--;}
        sum+=1/su;
        x=i-1;

    }
    printf("\n calculated e value:%lf\n",sum);

    return 0;
}
