

/**
 * Author: Zubeyir
 * Date:2023/1/2         
 * Source: Teacher Chris Bourke's book
 * Chapter 4: Loops
 * Example:Excercise 4.8
 * Aim: 
 * estimate the value of pi=3.1
 */


#include<stdio.h>
#include<stdlib.h>


int main(int argc, char ** argv){
    int a=0;
    printf("This is a a program that\n");
    printf("pi number finder so\n");
    printf("iteration number:");
    scanf("%d",&a);
    printf("\nEntered number:%d\n",a);
    
    int x=a;
    double sum=1.00; 
    double xyz=1.12;
    int r=a%2;
    int s=0;
    if(r==1){
        // negatives
        // evens
        s=(a-1)/2;
        for(int i=s;i>=1;i--){
            sum-=1/(2*i+1);
            }
        // odds
        x=x-1;
        for(int i=x;i>1;i--){
            sum+=1/(2*i+1);
            }
    }
    else{
        // positive
        // odds
        x=a/2;
        for(int i=x;i>1;i--){
            sum+=1/(2*i+1);
            }
        //evens
        s=(a/2)+1;
        for(int i=s;i>=1;i--){
            sum-=1/(2*i+1);
            }
    }

    printf("\n calculated pi value:%lf\n",sum);

    return 0;
}
